#! /bin/bash

read -p 'Install wifi drivers in package layer (rsncra/rtl88x2bu-kmod): [y/n]: ' wifi
if [ $wifi == 'y' ]; then
  echo "install wifi driveers (rsncra/rtl88x2bu-kmod)"
  cd /etc/yum.repos.d
  sudo wget https://copr.fedorainfracloud.org/coprs/rsncra/rtl88x2bu-kmod/repo/fedora-37/rsncra-rtl88x2bu-kmod-fedora-37.repo
  sudo rpm-ostree install rtl88x2bu-kmod
fi
