#! /bin/bash

echo "updating the whole system"
sudo pacman -Syu

read -p 'Install base-devel: [y/n]: ' bd
if [ $bd == 'y' ]; then
  echo "Installing base devel"
  sudo pacman -S base-devel
fi

read -p 'Enable aur in pamac [y/n]: ' aur
if [ $aur == 'y' ]; then
  echo 'uncommenting #EnableAUR from /etc/pamac.conf'
  sudo sed -Ei '/EnableAUR/s/^#//' /etc/pamac.conf
fi

read -p 'Install snap: [y/n]: ' snap
if [ $snap == 'y' ]; then
  echo "Installing snapd"
  sudo pacman -S snapd
  sudo systemctl enable --now snapd.socket
  sudo ln -s /var/lib/snapd/snap /snap
  systemctl enable --now snapd.apparmor
fi

read -p 'Install linux-header: [y/n]: ' headers
if [ $headers == 'y' ]; then
  echo "Installing linux-headers"
  sudo pacman -S linux-headers
fi

read -p 'install wifi drivers (rtl88x2bu-dkms-git): [y/n]: ' wifi
if [ $wifi == 'y' ]; then
  echo "install wifi driveers (rtl88x2bu-dkms-git)"
  sudo pamac install rtl88x2bu-dkms-git
fi

read -p 'Setup CUPS (printer services): [y/n]: ' printer
if [ $printer == 'y' ]; then
  echo "Installing KDE printer manager"
  sudo pamac install print-manager
  echo "Setting up CUPS"
  sudo pamac install manjaro-printer
  sudo gpasswd -a $(whoami) sys
  sudo systemctl enable --now cups.service
  sudo systemctl enable --now cups.socket
  sudo systemctl enable --now cups.path
  sudo pamac install avahi
  sudo systemctl enable --now avahi-daemon.service
fi

read -p 'Setting up Samba: [y/n]: ' samba

if [ $samba == 'y' ]; then
  echo "installing gvfs-smb"
  sudo pamac install gvfs-smb
  sudo pamac install samba kdenetwork-filesharing manjaro-settings-samba
fi

read -p 'install rust: [y/n]: ' rust

if [ $rust == 'y' ]; then
  echo "installing Rust"
  cd ~
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  source "$HOME/.cargo/env"
fi