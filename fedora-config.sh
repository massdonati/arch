#! /bin/bash

read -p 'Speedup dnf (max_parallel_downloads=10, fastestmirror=true): [y/n]: ' dnf
if [ $dnf == 'y' ]; then
  echo "Adding  dnf properties..."
  echo "max_parallel_downloads=10" | sudo tee -a /etc/dnf/dnf.conf
  echo "fastestmirror=true" | sudo tee -a /etc/dnf/dnf.conf
fi

read -p 'updating the whole system: [y/n]: ' update
if [ $update == 'y' ]; then
  echo "updating the whole system"
  sudo dnf update
fi

read -p 'Check size of root Volume: [y/n]: ' rootV
if [ $rootV == 'y' ]; then
  echo "running pvscan \n"
  sudo pvscan

  read -p 'Resize the root volume: [y/n]: ' rootV
  if [ $rootV == 'y' ]; then
    read -p 'New size: [10G to increase the volume by 10 gigabytes]: ' size
    echo "resizing..."
    sudo lvextend --size +$size --resizefs /dev/fedora/root
  fi
fi

read -p 'Enable RPM Fusion: [y/n]: ' rpm
if [ $rpm == 'y' ]; then
  echo "Installing rpm fusion..."
  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
  sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
fi

read -p 'Add the Flathub Repository: [y/n]: ' fh
if [ $fh == 'y' ]; then
  echo "Installing flathub..."
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
fi

read -p 'Install vlc and multimedia codec: [y/n]: ' vlc
if [ $vlc == 'y' ]; then
  echo "Installing Vlc + codecs..."
  sudo dnf install vlc
  sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
  sudo dnf install lame\* --exclude=lame-devel
  sudo dnf group upgrade --with-optional Multimedia
fi

read -p 'Change hostname [y/n]: ' hn
if [ $hn == 'y' ]; then
  read -p 'new hostname: ' hostname
  echo "Changing hostname..."
  sudo hostnamectl set-hostname "$hostname"
fi

read -p 'Install wifi drivers (rsncra/rtl88x2bu-kmod): [y/n]: ' wifi
if [ $wifi == 'y' ]; then
  echo "install wifi driveers (rsncra/rtl88x2bu-kmod)"
  sudo dnf copr enable rsncra/rtl88x2bu-kmod
  sudo dnf in rtl88x2bu-kmod
fi

read -p 'Install rust: [y/n]: ' rust
if [ $rust == 'y' ]; then
  echo "installing Rust..."
  cd ~
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  source "$HOME/.cargo/env"
fi

read -p 'Install snap: [y/n]: ' snap
if [ $snap == 'y' ]; then
  echo "Installing snapd..."
  sudo dnf install snapd
  sudo ln -s /var/lib/snapd/snap /snap
fi

read -p 'Install VS Code: [y/n]: ' code
if [ $code == 'y' ]; then
  echo "installing vscode..."
  snap install code --classic
fi

read -p 'Install Gnome extension/manager: [y/n]: ' ext
if [ $ext == 'y' ]; then
  echo "installing gnome extension and enxtension manager..."
  flatpak install flathub org.gnome.Extensions com.mattjakeman.ExtensionManager
fi

read -p 'Install Gnome screenshot: [y/n]: ' ext
if [ $ext == 'y' ]; then
  echo "installing gnome extension and enxtension manager..."
  sudo dnf install gnome-screenshot
fi